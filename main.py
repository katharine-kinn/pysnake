import os
import sys
import curses
import time
import random

from snake import Snake, is_direction_collinear


def generate_food(origin, height, width, snake_coordinates):
    """Returns a pair of coordinates for a next target
    """
    f = snake_coordinates[0]
    while f in snake_coordinates:
        f = (
                random.randint(origin[0], height),
                random.randint(origin[1], width)
            )
    return f


def init_game():
    snake = Snake()
    return snake


def get_snake_coordinates(snake):
    """Prepares a list of coordinates
        given snake head position,
        list of nodes and direction

        returns: list
    """
    coordinates = [snake.head]

    direction = snake.direction
    head = snake.head

    for i in range(1, snake.length):
        head = (
            head[0] - direction[0],
            head[1] - direction[1]
        )
        coordinates.append(head)
        for n in snake.nodes:
            if n.position == i:
                direction = n.direction
                break
    return coordinates



def draw(stdscr, snake_coordinates, food, score):
    """Draws snake, score and target
    """
    stdscr.clear()

    stdscr.addstr(1, 0, 'Score: {0}'.format(score))

    stdscr.addstr(
        food[0],
        food[1],
        '@'
    )

    for c in snake_coordinates:
        stdscr.addstr(
            c[0],
            c[1],
            '*'
        )


def loop(stdscr, snake):
    """Main loop of the game
    """
    curses.curs_set(0)
    stdscr.nodelay(True)

    display_center = (
        int((curses.LINES - 1) / 2),
        int((curses.COLS - 1) / 2)
    )
    snake.set_head(display_center)

    frame = 10
    food = generate_food(
        (frame, frame),
        curses.LINES - frame,
        curses.COLS - frame,
        get_snake_coordinates(snake)
    )

    while True:

        try:
            snake_coordinates = get_snake_coordinates(snake)
            score = snake.get_score()
            draw(stdscr, snake_coordinates, food, score)
        except curses.error:
            stdscr.clear()
            stdscr.addstr(display_center[0], display_center[1], 'Game over')

        user_input = stdscr.getch()

        direction = None
        if user_input == curses.KEY_UP:
            direction = (-1, 0)
        elif user_input == curses.KEY_DOWN:
            direction = (1, 0)
        elif user_input == curses.KEY_LEFT:
            direction = (0, -1)
        elif user_input == curses.KEY_RIGHT:
            direction = (0, 1)

        if (direction is not None 
            and not is_direction_collinear(direction, snake.direction)):
            snake.set_direction(direction)
            snake.move() 
            continue

        if food in snake_coordinates:
            snake.eat()
            food = generate_food(
                (frame, frame),
                curses.LINES - frame,
                curses.COLS - frame,
                snake_coordinates,
            )        

        snake.move()    
        time.sleep(0.15)


if __name__ == '__main__':
    snake = init_game()
    curses.wrapper(loop, snake)
