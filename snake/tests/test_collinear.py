import unittest

from snake import is_direction_collinear


class CollinearTestCase(unittest.TestCase):

    def test_is_direction_collinear(self):
        self.assertEqual(
            is_direction_collinear(
                (0, 1), (0, 1) 
            ), True)
        self.assertEqual(
            is_direction_collinear(
                (0, 1), (0, -1) 
            ), True)
        self.assertEqual(
            is_direction_collinear(
                (0, -1), (-1, 0) 
            ), False)
        self.assertEqual(
            is_direction_collinear(
                (0, 1), (-1, 0) 
            ), False)
        self.assertEqual(
            is_direction_collinear(
                (1, 0), (1, 0) 
            ), True)
        self.assertEqual(
            is_direction_collinear(
                (1, 0), (-1, 0) 
            ), True)
        self.assertEqual(
            is_direction_collinear(
                (0, 1), (1, 0) 
            ), False)
        self.assertEqual(
            is_direction_collinear(
                (2, 3), (4, 6) 
            ), True)
        self.assertEqual(
            is_direction_collinear(
                (2, 1), (4, 6) 
            ), False)
        self.assertEqual(
            is_direction_collinear(
                (1, 2), (4, 6) 
            ), False)


if __name__ == '__main__':
    unittest.main()
