
__all__ = ['Snake', 'is_direction_collinear']



def is_direction_collinear(direction_1, direction_2):
    """Checks whether given vectors are collinear

        :param tuple direction_1:
        :param tuple direction_2:
    """

    p = None
    for i in (0, 1):
        dividend = max(abs(direction_1[i]), abs(direction_2[i]))
        divider = min(abs(direction_1[i]), abs(direction_2[i]))
        if divider == 0:
            if dividend == 0:
                continue
            else:
                return False
        res = dividend / divider
        if p is None:
            p = res
            continue
        if p != res:
            return False

    return True 


class Node():

    def __init__(self, position, direction):
        super(Node, self).__init__()
        self.position = position
        self.direction = direction

    def __str__(self):
        return '{0}@{1}'.format(self.direction, self.position)


class Snake():

    init_length = 12
    
    def __init__(self, length=init_length):
        super(Snake, self).__init__()
        self.head = (0, 0)
        self.direction = (0, 1)
        self.nodes = []
        self.length = length

    def __str__(self):
        return '{0} -> {1} [{2}]'.format(self.head, self.direction, [str(x) for x in self.nodes])

    def _add_node(self, direction):
        node = Node(0, direction)
        self.nodes.insert(0, node)

    def _pop_node(self):
        del self.nodes[-1]

    def set_head(self, head):
        self.head = head

    def move(self):
        head = (
            self.head[0] + self.direction[0],
            self.head[1] + self.direction[1]
        )
        self.set_head(head)

        for n in self.nodes:
            n.position += 1

        if len(self.nodes) and self.nodes[-1].position >= self.length - 1:
            self._pop_node()

    def set_direction(self, direction):
        self._add_node(self.direction)
        self.direction = direction

    def eat(self):
        self.length += 1

    def get_score(self):
        return self.length - self.init_length


