import setuptools

description = None
with open('README.md', 'r') as readme_file:
    description = readme_file.read()

setuptools.setup(
    name='pysnake',
    version='0.01',
    author='Catherine',
    author_email='katherine.goncharova@gmail.com',
    description='Console snake game',
    long_description=description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/katharine-kinn/pysnake',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
